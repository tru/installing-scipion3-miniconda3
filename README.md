# Walktrought for installing scipion3 on bis.pasteur.fr machines

Feel free to adapt for other setup if needed (maestro, gpulab, ....)

```
/c7/shared/bin/install-miniconda3-HOME.sh
source $HOME/bin/enable-miniconda3-HOME.sh
module add cuda/11.7.0_515.43.04 openmpi/3.1.6-devtoolset-9
python3 -m pip install scipion-installer
python3 -m scipioninstaller $HOME/scipion3 -j 4
$HOME/scipion3/scipion3
```
Reference: https://scipion-em.github.io/docs/docs/scipion-modes/install-from-sources.html#installing-scipion-v3-0 (2022/06/24)


Tru <tru@pasteur.fr>


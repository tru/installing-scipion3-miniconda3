1) install miniconda3 (using x86_64 linux version here)
```
[tru@ld21-1022 ~]$ /c7/shared/bin/install-miniconda3-HOME.sh
PREFIX=/c7/home/tru/miniconda3
Unpacking payload ...
Collecting package metadata (current_repodata.json): done
Solving environment: done

<...>
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
/c7/home/tru/bin/enable-miniconda3-HOME.sh created, to activate this conda
installation:
source /c7/home/tru/bin/enable-miniconda3-HOME.sh
```

2) enable miniconda3 enable the required modules and install scipion-installer

You have the choice for openmi and cuda variants... just use a recent enough gcc suite
compiled openmpi version.

```
[tru@ld21-1022 ~]$ source $HOME/bin/enable-miniconda3-HOME.sh
(base) [tru@ld21-1022 ~]$ module add cuda/11.7.0_515.43.04 openmpi/3.1.6-devtoolset-9

(base) [tru@ld21-1022 ~]$ module li
Currently Loaded Modulefiles:
  1) slurm/current                2) scl/devtoolset-9             3) openmpi/3.1.6-devtoolset-9   4) cuda/11.7.0_515.43.04

(base) [tru@ld21-1022 ~]$ conda env list
# conda environments:
#
base                  *  /c7/home/tru/miniconda3

(base) [tru@ld21-1022 ~]$ python3 -m pip install scipion-installer
<skipped>
```
3) install scipion3 as stated in the doc
```
(base) [tru@ld21-1022 ~]$ python3 -m scipioninstaller $HOME/scipion3 -j 4
conda detected. Favouring it. If you want a virtualenv installation cancel installation and pass -venv .
Collecting package metadata (current_repodata.json): done
Solving environment: done
<skipped>
  ********************************************
  *                                          *
  *  Xmipp has been successfully installed!  *
  *                                          *
  ********************************************


/c7/home/tru/scipion3/software/em/xmippSrc-v3.22.04.0
cd /c7/home/tru/scipion3/software/em/xmippSrc-v3.22.04.0
find /c7/home/tru/scipion3/software/em/xmipp/bindings/python -maxdepth 1 -mindepth 1 ! -name __pycache__ -exec ln -srfn {} /c7/home/tru/scipion3/software/bindings \; && ln -srfn /c7/home/tru/scipion3/software/em/xmipp/lib/libXmippCore.so /c7/home/tru/scipion3/software/lib && touch bindings_linked && rm installation_finished 2> /dev/null
cd /c7/home/tru/scipion3/software/em/
Link 'xmippSrc-3.22.04.0 -> xmippSrc-v3.22.04.0'
Created link: 'xmippSrc-3.22.04.0 -> xmippSrc-v3.22.04.0'
Done (9 m 52 s)

 ______________________________________________________________________________
|                                                                              |
| Installation successfully finished!! Happy EM processing!!                   |
 ______________________________________________________________________________

  You can launch Scipion using the launcher at: /c7/home/tru/scipion3/scipion3
 ______________________________________________________________________________
(base) [tru@ld21-1022 ~]$
```
4) enjoy
```
$HOME/scipion3/scipion3
```
